<?php
namespace App\Services;

use App\Rate;
use Carbon\Carbon;

class ExchangeRateService
{
    private $from = '';
    private $to = '';
    
    public function __construct(string $from, string $to)
    {
       $this->from = $from;
       $this->to = $to;
    }
    
    /**
    * Validate currency codes
    * @return voids
    * @throws \Exception
    */
    private function validate()
    {
       // validate currency codes
       if (!in_array(strtoupper($this->from), config('app.currencies'))) {
            throw \Exception('currency code ' . $this->from . ' not supported');
       }
       if (!in_array(strtoupper($this->to), config('app.currencies'))) {
            throw \Exception('currency code ' . $this->to . ' not supported');
       }
    }
    
    /**
     * Exchange currency
     * @param int $amount
     * @return array
     */
    public function exchange(int $amount): array
    {   
        // validate input data
        $this->validate();
        
        $fromCache = 1;
        
        $fromPeriod =  Carbon::now()->subHours(env('EXPIRE_CACHE'))->toDateTimeString();
        
        $rate = Rate::getRate($this->from, $this->to, $fromPeriod)->first();
     
        if (!$rate) {
            $this->refreshRates();
            $fromCache = 0;
        }
        
        return ['rate' => number_format($rate->rate*$amount,2,'.', ''), 'fromCache' => $fromCache];
    }
    
    /**
     * Get rates from external API
     * @return array
     */
    public function pullRates(): array
    {
        $resultArr = [];
        $client = new \GuzzleHttp\Client();
        
        $result = $client->request('GET', env('RATE_API_URL'));
        $resultArr = json_decode($result->getBody()->getContents(), true);
        
        return $resultArr;
    }
    
    /**
     * Refresh currency rates
     * @return void
     */
    public function refreshRates()
    {
        $rates = $this->pullRates();
        foreach ($rates['rates'] as $currency => $rate) {
            // save only the currencies we support
            if (!in_array(strtoupper($currency), config('app.currencies'))) {
                continue;
            }
            $rateData = [
                'FROM' => strtoupper($rates['base']) ?? 'EUR',
                'TO'   => strtoupper($currency),
                'rate' => $rate 
            ];
            $model = new Rate();
            $model->create($rateData);
        }
        
    }
}
