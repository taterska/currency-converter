<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'FROM', 'TO', 'rate'
    ];
    
    protected $table = 'rates';
    
    public function scopeGetRate($query, $from, $to, $period)
    {
        return $query->select('rate')
                ->where(function($query) use ($from, $to) {
                    return $query->where('FROM', '=', $from)
                        ->where('TO', '=', $to);
                })
                ->orWhere(function($query) use ($from, $to) {
                    return $query->where('TO', '=', $from)
                        ->where('FROM', '=', $to);
                })
                 ->where('created_at', '>', 
                    $period
                );
                ;
    }
}
