<?php

/* 
 * Supported currencies config
 */

return [
    'currencies' => [
    'CAD', 
    'JPY', 
    'USD', 
    'GBP', 
    'EUR', 
    'RUB', 
    'HKD', 
    'CHF'
        ]
];