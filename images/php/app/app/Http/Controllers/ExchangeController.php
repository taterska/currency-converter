<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Services\ExchangeRateService;

class ExchangeController extends BaseController
{

    /**
     * Calculate exchange currency
     * @param Request $request
     * @param int $amount
     * @param string $from
     * @param string $to
     * @return json 
     */
    public function exchange(Request $request, int $amount, string $from, string $to)
    {
        $service = new ExchangeRateService($from, $to);
        try {
            $rate = $service->exchange($amount); 
        } catch (\Exception $e) {
            return response()->json(['error' => 1, 'message' => 'Could not find rate']);   
        }
        
        return response()->json(['error' => 0, 'amount' => $rate['rate'], 'fromCache' => $rate['fromCache']]);
    }
    
    /**
     * Provides info about the API developer
     * @return json
     */
    public function info()
    {
        return response()->json(['error' => 0, 'API written by Ekaterina Petkova']);
    }
    
}
