<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Rate;

class CacheController extends BaseController
{

    public function clear()
    {
        Rate::query()->delete();
        return response()->json(['error' => 0, 'msg' => 'OK']);
    }
    
}
